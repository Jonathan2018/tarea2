package com.a4c.velizbarciajonathan.tarea2.datalevel;

public class DataException extends Exception
{
    public DataException() { super(); }
    public DataException(String message) { super(message); }
    public DataException(String message, Throwable cause) { super(message, cause); }
    public DataException(Throwable cause) { super(cause); }
}
