package com.a4c.velizbarciajonathan.tarea2.datalevel;

import java.util.ArrayList;


public interface FindCallback<DataObject> {
    public void done(ArrayList<DataObject> objects, DataException e);
}
