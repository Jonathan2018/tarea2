package com.a4c.velizbarciajonathan.tarea2.datalevel;


public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
